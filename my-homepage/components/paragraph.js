import styled from '@emotion/styled'

const Paragraph = styled.p`
    text-align: justify;
    text-align: 1em;
`

export default Paragraph
